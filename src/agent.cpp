/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "agent.h"
#include <vector>
using namespace std;

Agent::Agent(){}
Agent::~Agent()
{
  Stop();
}

void Agent::Start(){}
void Agent::Stop(){}
void Agent::Update(){}
void Agent::Input(){}
bool Agent::isAlive(){return true;}
float Agent::getScore(){return 0.0;}

GeneticAgent::GeneticAgent():Agent(){}
GeneticAgent::~GeneticAgent()
{
    delete dna;
    delete brain;
}

void GeneticAgent::Configure(string layers, DNA* _dna, string _name)
{
  name = _name;

  brain = new Brain(layers);

  dna = _dna;
  if(dna == nullptr) dna = DNA::getRandDNA(layers);

  brain->applyDNA(dna);
}

void GeneticAgent::BrainInput(){}
void GeneticAgent::Input()
{
  BrainInput();
  brain->Update();
}
