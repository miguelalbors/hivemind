/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef HIVE_AGENT_H
#define HIVE_AGENT_H

#include "brain.h"

class Agent
{
  public:
    Agent();
    virtual ~Agent();

    virtual void Start();
    virtual void Update();
    virtual void Stop();
    virtual void Input();

    virtual bool isAlive();
    virtual float getScore();

    string name = "";

};

class GeneticAgent : public Agent
{
  public:
    GeneticAgent();
    virtual ~GeneticAgent();

    virtual void Input();
    void Configure(string layers, DNA* _dna = nullptr, string _name = "");

    virtual void BrainInput();

    DNA *dna;
    Brain *brain;

};

#endif //HIVE_AGENT_H
