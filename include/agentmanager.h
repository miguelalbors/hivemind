/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef HIVE_AGENTMANAGER_H
#define HIVE_AGENTMANAGER_H

#include "agent.h"
#include "dna.h"

#include <type_traits>
#include <vector>
#include <list>
#include <dirent.h>
#include <cstring>
using namespace std;

extern string folder;

class AgentManager
{
  public:
    static AgentManager *instance;

    template<class T = AgentManager>
    static T* getInstance()
    {
      static_assert(std::is_base_of<AgentManager, T>::value, "T must derive from AgentManager");
      return reinterpret_cast<T*>(instance);
    }

    AgentManager(bool _clearAtEnd = true);
    virtual ~AgentManager(){}

    template<class T>
    static T* addAgent()
    {
      static_assert(std::is_base_of<Agent, T>::value, "T must derive from Agent");
      T* tmp = new T();
      instance->agents.push_back(tmp);
      return tmp;
    }

    template<class T = Agent>
    static T* getAgent(int agent)
    {
      static_assert(std::is_base_of<Agent, T>::value, "T must derive from Agent");
      return reinterpret_cast<T*>(instance->agents[agent]);
    }

    static void Start();
    static void Update();
    static void Stop();
    virtual void stop();

    static bool stillAlive();

    void ordenaAgentes();
    void insertaOrdenado(int agente);

    list<int> resultados;
    vector<Agent*> agents;
    bool clearAgentsAtEnd = true;
};

template<class T = GeneticAgent>
class GeneticAgentManager : public AgentManager
{
  public:
    GeneticAgentManager(int poblacion = 100, int reproduction = 10, int repoblation = 50):AgentManager()
    {
      population = poblacion;
      reproductionPercent = (reproduction >= 0 && reproduction <= 100) ? reproduction : 10;
      repoblationPercent = (repoblation >= 0 && repoblation <= 100) ? repoblation : 50;
    }
    virtual ~GeneticAgentManager(){}

    virtual void stop()
    {
      //Limpiamos listas de mejores
        best.clear();

      //Ordenamos los agentes por resultado
        ordenaAgentes();

      cout << "[I] La mejor puntuación de la generación " << generation << " ha sido " << agents[resultados.front()]->getScore() << endl;
      getAgent<T>(resultados.front())->dna->parseToFile(folder + "/" + to_string(generation));

      //Calculamos cuántos se van a reproducir
        int to_reproduce = reproductionPercent * population / 100;
        if(to_reproduce <= 0) to_reproduce = 1;

        int reproduction_share = (repoblationPercent * population / 100) / to_reproduce;

      //Obtenemos el ADN de los mejores
        for(int i = 0; i < to_reproduce; i++)
        {
          best.push_back(getAgent<T>(resultados.front())->dna->getCopy());
          resultados.pop_front();
        }

      //Eliminamos los agentes
        AgentManager::stop();

      //Reproducimos los mejores
        for(int i = 0; i < best.size(); i++)
        {
          addAgent<T>()->Configure(brainConf, best[i]);
          for(int j = 1; j < reproduction_share && agents.size() < population; j++)
          {
            addAgent<T>()->Configure(brainConf, best[i]->getChild());
          }
        }

      //Rellenamos con nueva población hasta alcanzar la población deseada
        int cont = agents.size();
        while(agents.size() < population)
        {
          addAgent<T>()->Configure(brainConf);
        }

      cout << "[I] La nueva población es de " << agents.size() << " agentes, de los cuales " << best.size() << " son de generaciones anteriores, " << (reproduction_share*to_reproduce)-best.size() << " son descendientes y " << agents.size()-cont << " son nuevos" << endl;
      generation++;
    }

    static T* Initialize(string layers, string folder = "")
    {
      int pob = AgentManager::getInstance<GeneticAgentManager>()->population;
      AgentManager::getInstance<GeneticAgentManager>()->brainConf = layers;

      if(folder != "")
      {
        DIR *dir = opendir(folder.c_str());
        struct dirent *ent;
        string name;

        DNA *dna;
        while((ent = readdir(dir)) != nullptr && instance->agents.size() < pob)
        {
          if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) && (strcmp(ent->d_name, ".gitignore")!=0))
          {
            name = ent->d_name;
            const size_t pos = name.find_last_of('.');
            name.erase(pos);

            dna = new DNA();
            dna->parseFromFile(folder + "/" + name);
            GeneticAgentManager::addAgent<T>()->Configure(AgentManager::getInstance<GeneticAgentManager>()->brainConf, dna);
          }
        }

        if(instance->agents.size() > 0 && instance->agents.size() < pob)
          cout << "[I] Se van a crear " << pob - instance->agents.size() << " más para completar la población" << endl;
      }

      for(int i = instance->agents.size(); i < pob; i++)
      {
        GeneticAgentManager::addAgent<T>()->Configure(AgentManager::getInstance<GeneticAgentManager>()->brainConf);
      }
    }

    vector<DNA*> best;
    int population;
    int reproductionPercent = 10;
    int repoblationPercent = 50;

    int generation = 0;
    string brainConf;
};

#endif //HIVE_AGENTMANAGER_H
