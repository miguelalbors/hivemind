/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "brain.h"
#include "utils/parser.h"
#include <stdarg.h>

Brain::Brain(int n, ...)
{
  BrainLayer *last = nullptr;
  va_list vl;
  va_start(vl,n);

  for(int i = 0; i < n; i++)
  {
    last = new BrainLayer(va_arg(vl,int), last, i);
    layers.push_back(last);
  }

  va_end(vl);
}

Brain::Brain(string layerConf)
{
  BrainLayer *last = nullptr;

  int n;
  Parser *parser = new Parser(layerConf, P_RAW);
  for(Json::Value layer : parser->jsvalue["layers"])
  {
    Parser::json_int(layer, n);

    last = new BrainLayer(n, last, layers.size());
    layers.push_back(last);
  }

  delete parser;
}

void Brain::applyDNA(DNA *dna)
{
  for(int i = 0; i < dna->genome.size(); i++)
  {
    addConnection(dna->genome[i].layer, dna->genome[i].n, new Connection(dna->genome[i].n2, dna->genome[i].W, dna->genome[i].bias));
  }
}

void Brain::addConnection(int layer, int n, Connection *connection)
{
  layers[layer]->addConnection(n, connection);
}

void Brain::setBias(int layer, int n, float bias)
{
  layers[layer]->setBias(n, bias);
}

void Brain::Update()
{
  for(BrainLayer *layer : layers)
  {
    layer->Update();
  }
}

void Brain::print()
{
  for(BrainLayer *layer : layers)
  {
    layer->print();
  }
}
