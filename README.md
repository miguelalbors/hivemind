# Hive Mind
Hive Mind es una librería para implementar redes neuronales basadas en algoritmos genéticos de una manera cómoda.

## Clases
|Nombre               |Descripción  | Localización |
| ---                 | ---         | ---          |
| Agent               | Clase que implementa el funcionamiento base de los agentes | agent.h |
| GeneticAgent        | Clase hija de `Agent` que integra en el agente un `Brain` y un `DNA` | agent.h |
| AgentManager        | Clase que implementa el funcionamiento base del gestor de agentes | agentmanager.h |
| GeneticAgentManager | Clase hija de `AgentManager` que integra en el gestor de agentes la reproducción de nuevas generaciones | agentmanager.h |
| Brain               | Clase que implementa la red neuronal | brain.h |
| BrainLayer          | Clase que implementa las capas de neuronas | brainlayer.h|
| Neuron              | Estructura para generar neuronas | brainlayer.h |
| Connection          | Estructura para generar conexiones entre neuronas | brainlayer.h |
| DNA                 | Clase que implementa el funcionamiento del ADN | dna.h |
| Gen                 | Estructura para generar genes | dna.h |
