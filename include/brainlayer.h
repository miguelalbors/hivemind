/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef HIVE_BRAINLAYER_H
#define HIVE_BRAINLAYER_H

#include <iostream>
#include <vector>
using namespace std;

#define MAX(a,b) ((a > b)? a : b)

class BrainLayer;

struct Connection
{
  float W, bias;
  int n;

  Connection():n(0), W(0.0), bias(0.0){}
  Connection(int _n, float _W, float _bias):n(_n), W(_W), bias(_bias){}
};

struct Neuron
{
  float bias, value;
  vector<Connection*> connections;

  Neuron():bias(0.0), value(0.0){}
  Neuron(float _bias):bias(_bias), value(0.0){}

  void updateValue(BrainLayer* prevLayer = nullptr);
};

class BrainLayer
{
  public:
    BrainLayer(int amount, BrainLayer* _prevLayer = nullptr, int _id = 0);
    virtual ~BrainLayer(){}

    void addConnection(int n, Connection *connection);
    void setBias(int n, float bias);
    float getNeuronValue(int n);

    void Update();

    vector<Neuron*> neurons;
    int id;

    BrainLayer* prevLayer = nullptr;

    void print();
};

#endif //HIVE_BRAINLAYER_H
