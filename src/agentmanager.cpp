/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "agentmanager.h"

string folder = "saves";

AgentManager* AgentManager::instance = nullptr;

AgentManager::AgentManager(bool _clearAtEnd)
{
  if(instance != nullptr)
  {
    delete this;
    return;
  }

  instance = this;
  clearAgentsAtEnd = _clearAtEnd;
}

void AgentManager::Start()
{
  for(Agent* agent : instance->agents)
  {
    agent->Start();
  }
}

void AgentManager::Update()
{
  for(Agent* agent : instance->agents)
  {
    agent->Input();
    agent->Update();
  }
}

void AgentManager::Stop()
{
  instance->stop();
}
void AgentManager::stop()
{
  if(clearAgentsAtEnd)
  {
    for(Agent* agent : agents)
    {
      delete agent;
    }

    instance->agents.clear();
  }
  else
  {
    for(Agent* agent : agents)
    {
      agent->Stop();
    }
  }
}

bool AgentManager::stillAlive()
{
  if(instance->agents.size() > 0)
  {
    for(Agent* agent : instance->agents)
    {
      if(agent->isAlive())
      return true;
    }
  }

  return false;
}

void AgentManager::ordenaAgentes()
{
  resultados.clear();
  for(int i = 0; i < agents.size(); i++)
  {
    insertaOrdenado(i);
  }
}

void AgentManager::insertaOrdenado(int agente)
{
  list<int> newList;

  bool insertado = false;

  for(int i : resultados)
  {
    if(!insertado && agents[agente]->getScore() > agents[i]->getScore())
    {
      newList.push_back(agente);
      insertado = true;
    }

    newList.push_back(i);
  }

  if(!insertado)
    newList.push_back(agente);

  resultados = list<int>(newList);
}
