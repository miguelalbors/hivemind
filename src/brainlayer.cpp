/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "brainlayer.h"

void Neuron::updateValue(BrainLayer* prevLayer)
{
  value = bias;
  for(Connection* c : connections)
  {
    value += prevLayer->getNeuronValue(c->n) * c->W + c->bias;
  }

  value = MAX(0, value);
}

BrainLayer::BrainLayer(int amount, BrainLayer* _prevLayer, int _id)
{
  id = _id;
  prevLayer = _prevLayer;
  for(int i = 0; i < amount; i++)
  {
    neurons.push_back(new Neuron());
  }
}

void BrainLayer::addConnection(int n, Connection *connection)
{
  neurons[n]->connections.push_back(connection);
}

void BrainLayer::setBias(int n, float bias)
{
  neurons[n]->bias = bias;
}

float BrainLayer::getNeuronValue(int n)
{
  return neurons[n]->value;
}

void BrainLayer::Update()
{
  for(Neuron* n : neurons)
  {
    n->updateValue(prevLayer);
  }
}


void BrainLayer::print()
{
  cout << "BrainLayer " << id << endl;
  cout << "  Size: " << neurons.size() << endl;

  int i = 0;
  for(Neuron* n : neurons)
  {
    cout << "    > " << i << endl;
    cout << "        bias:" << n->bias << endl;
    cout << "       value:" << n->value << endl;

    for(Connection *c : n->connections)
    {
      cout << "       is connected to " << c->n << "(" << c->W << ")" << endl;
    }

    i++;
  }
}
