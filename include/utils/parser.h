#ifndef HIVE_PARSER_H
#define HIVE_PARSER_H

#include "./json.h"

#include <string>
using namespace std;

enum ParserOpenMode
{
  P_RAW = 0,
  P_FILE = 1
};

class Parser
{
  public:
    Parser(string content, int mode = P_FILE);
    virtual ~Parser();

    static bool json_string(Json::Value & o, string & res);
    static bool json_float(Json::Value & o, float &res);
    static bool json_int(Json::Value & o, int & res);
    static bool json_bool(Json::Value & o, bool & res);

  	Json::Value jsvalue;
    bool loaded = false;
};

#endif //HIVE_PARSER_H
