#include "utils/parser.h"

#include <iostream>
#include <fstream>

bool Parser::json_string(Json::Value & o, string & res)
{
	if (o.isNull()) return false;
	if (!o.isString()) return false;
	res = o.asString();
	return true;
}

bool Parser::json_float(Json::Value & o, float &res)
{
	if (o.isNull()) return false;
	if (!o.isDouble()) return false;
	res = o.asFloat();
	return true;
}

bool Parser::json_int(Json::Value & o, int & res)
{
	if (o.isNull()) return false;
	if (!o.isIntegral()) return false;
	res = o.asInt();
	return true;
}

bool Parser::json_bool(Json::Value & o, bool & res)
{
	if (o.isNull()) return false;
	if (!o.isBool()) return false;
	res = o.asBool();
	return true;
}

Parser::Parser(string content, int mode)
{
	switch(mode)
	{
		case P_RAW:
		{
			stringstream ss(content);
		  ss >> jsvalue;
			break;
		}

		case P_FILE:
		{
		  std::ifstream is;
			is.open(content.c_str(), std::ifstream::in);

			if(!is)
		  {
				cerr << "[E] Can't open " + content << endl;
		    return;
			}

			is >> jsvalue;
			break;
		}
	}

	loaded = true;
}

Parser::~Parser()
{

}
