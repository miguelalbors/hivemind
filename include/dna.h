/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef HIVE_DNA_H
#define HIVE_DNA_H

#include <vector>
#include <string>
using namespace std;


struct Gen
{
  float W, bias;
  int layer, n, n2;

  Gen():layer(0), n(0), n2(0), W(0.0), bias(0.0){}
  Gen(int _layer, int _n, int _n2, float _W, float _bias):layer(_layer), n(_n), n2(_n2), W(_W), bias(_bias){}
};

class DNA
{
  public:
    //Brain(int amount, int* neuronAmount);
    DNA(int _mutability = 0);
    virtual ~DNA(){}

    void addGen(Gen gen);
    DNA* getChild();
    DNA* getCopy();
    void parseToFile(string file);
    void parseFromFile(string file);

    int mutability = 0;
    vector<Gen> genome;

    static DNA* getRandDNA(int n, ...);
    static DNA* getRandDNA(string layers);
};

#endif //HIVE_DNA_H
