/*
  HIVE MIND
  GNU GPL v.3 - 2022 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "dna.h"
#include "utils/parser.h"

#include <sys/stat.h>
#include <stdarg.h>
#include <cstdlib>
#include <fstream>

DNA::DNA(int _mutability)
{
  mutability = _mutability;
}

void DNA::addGen(Gen gen)
{
  genome.push_back(gen);
}

DNA* DNA::getCopy()
{
  DNA* copy = new DNA(mutability);
  copy->genome = vector<Gen>(genome);
  return copy;
}

DNA* DNA::getChild()
{
  DNA* child = new DNA();
  child->mutability = mutability + (rand()%7) - 3;
  if(child->mutability < 0) child->mutability = 0;

  child->genome = vector<Gen>(genome);

  int gen_to_mute, type_of_mutation;
  for(int i = 0; i < mutability; i++)
  {
    gen_to_mute = rand()%child->genome.size();
    type_of_mutation = rand()%100;

    if(type_of_mutation <= 85)
      child->genome[gen_to_mute].W = child->genome[gen_to_mute].W + (-0.25 + ((float)(rand()%51)/100.0));
    else
      child->genome[gen_to_mute].bias = child->genome[gen_to_mute].bias + (-0.25 + ((float)(rand()%51)/100.0));
  }

  return child;
}

void DNA::parseToFile(string file)
{
  struct stat buffer;
  //Si no existe el fichero, lo creamos
    if(stat((file + ".json").c_str(), &buffer) != 0)
    {
      ofstream s(file + ".json");
      s << "{}";
      s.close();
    }

  Parser *saveParser = new Parser(file + ".json");
  saveParser->jsvalue["mutability"] = mutability;

  int i = 0;
  for(Gen gen : genome)
  {
    saveParser->jsvalue["genome"][i][0] = gen.layer;
    saveParser->jsvalue["genome"][i][1] = gen.n;
    saveParser->jsvalue["genome"][i][2] = gen.n2;
    saveParser->jsvalue["genome"][i][3] = gen.W;
    saveParser->jsvalue["genome"][i][4] = gen.bias;
    i++;
  }

  ofstream salida(file + ".json");
  Json::StyledStreamWriter writer;
  writer.write(salida, saveParser->jsvalue);

  delete saveParser;
}

void DNA::parseFromFile(string file)
{
  struct stat buffer;
  //Si no existe el fichero, nos vamos
    if(stat((file + ".json").c_str(), &buffer) != 0)
    {
      return;
    }

  Parser *loadParser = new Parser(file + ".json");
  genome.clear();

  Parser::json_int(loadParser->jsvalue["mutability"], mutability);

  for(Json::Value gen : loadParser->jsvalue["genome"])
  {
    Gen g;
    Parser::json_int(gen[0], g.layer);
    Parser::json_int(gen[1], g.n);
    Parser::json_int(gen[2], g.n2);
    Parser::json_float(gen[3], g.W);
    Parser::json_float(gen[4], g.bias);
    genome.push_back(g);
  }
  delete loadParser;
}

DNA* DNA::getRandDNA(int n, ...)
{
  string layers = "{\"layers\": [";

  va_list vl;
  va_start(vl,n);
  for(int i = 1; i < n; i++)
  {
    layers = layers + ((i > 1)?",":"") + to_string(va_arg(vl,int));
  }
  layers = "]}";

  va_end(vl);
  return getRandDNA(layers);
  /*DNA *dna = new DNA();

  va_list vl;
  va_start(vl,n);

  int lastN = va_arg(vl,int);
  int actualN;
  for(int i = 1; i < n; i++)
  {
    actualN = va_arg(vl,int);

    for(int n1 = 0; n1 < actualN; n1++)
    {
      for(int n2 = 0; n2 < lastN; n2++)
      {
        dna->addGen(Gen(i, n1, n2, -1.0 + ((float)(rand()%201)/100.0), -1.0 + ((float)(rand()%201)/100.0)));
      }
    }

    lastN = actualN;
  }

  va_end(vl);

  dna->mutability = 5 + rand()%(dna->genome.size()/2);
  return dna;*/
}

DNA* DNA::getRandDNA(string layers)
{
  DNA *dna = new DNA();

  int lastN, actualN, i=0;

  Parser *parser = new Parser(layers, P_RAW);
  for(Json::Value layer : parser->jsvalue["layers"])
  {
    Parser::json_int(layer, actualN);

    if(i > 0)
    {
      for(int n1 = 0; n1 < actualN; n1++)
      {
        for(int n2 = 0; n2 < lastN; n2++)
        {
          dna->addGen(Gen(i, n1, n2, -1.0 + ((float)(rand()%201)/100.0), -1.0 + ((float)(rand()%201)/100.0)));
        }
      }
    }

    i++;
    lastN = actualN;
  }

  dna->mutability = 5 + rand()%(dna->genome.size()/2);
  return dna;
}
